var hierarchy =
[
    [ "yami::core::agent", "classyami_1_1core_1_1agent.html", null ],
    [ "yami::core::channel_descriptor", "classyami_1_1core_1_1channel__descriptor.html", null ],
    [ "yami::core::parameter_entry", "classyami_1_1core_1_1parameter__entry.html", null ],
    [ "yami::core::parameter_iterator", "classyami_1_1core_1_1parameter__iterator.html", null ],
    [ "yami::core::serializable", "classyami_1_1core_1_1serializable.html", [
      [ "yami::core::parameters", "classyami_1_1core_1_1parameters.html", null ],
      [ "yami::core::raw_buffer_data_source", "classyami_1_1core_1_1raw__buffer__data__source.html", null ]
    ] ]
];